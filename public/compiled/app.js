var app = angular.module('PublisherManager', ['infinite-scroll']);
;(function() {
    angular.module('PublisherManager')
        .controller('networkListController', NetworkListController);

    NetworkListController.$inject = ['$scope', 'publisherService'];
    function NetworkListController(scope, publisherService) {
        var vm = scope;

        vm.init = init;
        vm.edit = edit;
        vm.cancelEdit = cancelEdit;
        vm.save = save;
        vm.deleteNetwork = deleteNetwork;
        vm.add = add;
        vm.networks = [];

        function init(networks) {
            vm.networks = networks;
        }

        function deleteNetwork(network) {
            if (confirm('delete network ' + network.name + '?')) {
                network.saving = true;
                publisherService.deleteNetwork(network.id)
                    .success(function(result) {
                        network.is_deleted = true;
                        network.saving = false;
                    });
            }
        }

        function edit(network) {
            network.edit = true;
            network.originalPattern = network.pattern;
            network.originalName = network.name;
        }

        function cancelEdit(network) {
            if (!network.id) {
                vm.networks.splice(vm.networks.length - 1, 1);
                return;
            }
            network.edit = false;
            network.pattern = network.originalPattern ? network.originalPattern : network.pattern;
            network.name = network.originalName ? network.originalName : network.name;
        }

        function add() {
            vm.networks.push({
                name : 'new network',
                pattern : '',
                edit : true
            });
        }

        function save(network) {
            network.edit = false;
            network.saving = true;
            publisherService.saveNetwork(network)
                .success(function(result) {
                    if (!network.id) {
                        network.id = result.id;
                    }
                    network.name = result.name;
                    network.pattern = result.pattern;
                    network.originalPattern = network.pattern;
                    network.originalName = network.name;
                    network.saving = false;
                });
        }
    }
})();
;(function() {
    angular.module('PublisherManager')
        .constant('PUBLISHERS_API', {
            checkNetworkAndAdultWords: '/api/checkNetworkAndAdultWords',
            markSiteAdult: '/api/markSiteAdult',
            saveNetwork: '/api/network/save',
            deleteNetwork: '/api/network/delete',
            findSites: '/api/sites/find'
        })
        .factory('publisherService', PublisherService);

    PublisherService.$inject = ['PUBLISHERS_API', '$http'];
    function PublisherService(publishersApi, http) {
        return {
            markSiteAdult: markSiteAdult,
            checkNetworkAndAdultWords: checkNetworkAndAdultWords,
            saveNetwork: saveNetwork,
            deleteNetwork: deleteNetwork,
            findSites: findSites
        };

        function checkNetworkAndAdultWords(urls, network) {
            return http.post(publishersApi.checkNetworkAndAdultWords, {urls: urls, network: network});
        }

        function markSiteAdult(url, isAdult) {
            return http.post(publishersApi.markSiteAdult, {url: url, isAdult: isAdult});
        }

        function saveNetwork(network) {
            return http.post(publishersApi.saveNetwork, network);
        }

        function deleteNetwork(networkId) {
            return http.post(publishersApi.deleteNetwork, {id: networkId});
        }

        function findSites(filter) {
            return http.post(publishersApi.findSites, filter);
        }
    }

})();
;(function() {
    angular.module('PublisherManager')
        .controller('publishersListController', PublishersListController);

    PublishersListController.$inject = ['$scope', 'publisherService'];
    function PublishersListController(scope, publisherService) {
        var vm = scope;

        vm.init = init;
        vm.nextPage = nextPage;
        vm.showPublisherInfo = showPublisherInfo;

        vm.networks = [];
        vm.publishers = [];
        vm.showAdult = false;
        vm.showNotAdult = true;
        vm.networkId = null;

        vm.pagination = {
            offset: 0,
            limit: 50
        };
        vm.loading = false;

        var allLoaded = false;

        function showPublisherInfo(publisher) {
            console.log(publisher);
            window.open('/manager/publisher/' + publisher.id, '_blank').focus();
        }

        function addPublisher(site) {
            vm.publishers.push({
                id: site.id,
                url: site.url,
                isAdult: site.is_adult != '0',
                adultWords: site.adult_words
            });
        }

        function handleLoadedPublishers(result) {
            for (var key in result.sites) {
                if (result.sites.hasOwnProperty(key)) {
                    addPublisher(result.sites[key]);
                }
            }
            vm.pagination.total = result.total;
            if (result.total >= result.offset + result.limit) {
                vm.pagination.offset += result.limit;
            } else {
                allLoaded = true;
            }
        }

        function loadPublishers() {
            if (vm.loading) return;
            vm.loading = true;
            publisherService.findSites({
                networkId: vm.networkId,
                showAdult: vm.showAdult,
                showNotAdult: vm.showNotAdult,
                limit: vm.pagination.limit,
                offset: vm.pagination.offset
            })
                .success(function(result) {
                    handleLoadedPublishers(result);
                    vm.loading = false;
                });
        }

        function nextPage() {
            if (!allLoaded) {
                loadPublishers();
            }
        }

        function onFilterChanged(newValue, oldValue) {
            if (newValue != oldValue) {
                vm.pagination.offset = 0;
                vm.pagination.total = 0;
                allLoaded = false;
                vm.publishers = [];
                loadPublishers();
            }
        }

        function init(networks) {
            loadPublishers();

            vm.networks = networks;

            vm.$watch('networkId', onFilterChanged);
            vm.$watch('showAdult', onFilterChanged);
            vm.$watch('showNotAdult', onFilterChanged);
        }
    }
})();
;(function() {
    angular.module('PublisherManager')
        .controller('siteListUploadController', SiteListUploadController);

    SiteListUploadController.$inject = ['$scope', 'publisherService'];
    function SiteListUploadController(scope, publisherService) {
        var vm = scope;

        var sizeWebsitesByRequest = 20;
        var allNetworks = [];

        vm.init = init;
        vm.change = change;
        vm.cancelChange = cancelChange;
        vm.save = save;
        vm.checkAgain = checkAgain;

        vm.statuses = {
            'new' : 'new',
            checked : 'checked',
            error : 'error'
        };

        vm.network = null;
        vm.websites = [];
        vm.switchNetworkId = '';

        function save(website) {
            publisherService.markSiteAdult(website.url, website.is_adult)
                .success(function(result) {
                    updateWebsite(website, result);
                    website.is_changed = false;
                });
        }

        function cancelChange(website) {
            website.is_changed = false;
            website.is_adult = website.server_is_adult;
        }

        function change(website) {
            website.is_changed = true;
        }

        function getWebsiteByUrl(url) {
            for (var key in vm.websites) {
                if (vm.websites.hasOwnProperty(key)) {
                    if (vm.websites[key].url == url) {
                        return vm.websites[key];
                    }
                }
            }
            return null;
        }

        function updateWebsite(website, data) {
            website.id = data.id;
            website.is_adult = data.is_adult;
            website.server_is_adult = data.is_adult;
            website.adult_words = data.adult_words;
        }

        function handleError(urls, error) {
            for (var key in urls) {
                if (urls.hasOwnProperty(key)) {
                    var website = getWebsiteByUrl(urls[key]);
                    if (website) {
                        website.status = vm.statuses.error;
                        website.error = error;
                    }
                }
            }
        }

        function handleCheckNetworkResult(checkNetworkAndAdultWordsResult) {
            for (var url in checkNetworkAndAdultWordsResult) {
                if (checkNetworkAndAdultWordsResult.hasOwnProperty(url)) {
                    var perSiteResult = checkNetworkAndAdultWordsResult[url];
                    var website = getWebsiteByUrl(url);
                    if (website) {
                        if (perSiteResult.site) {
                            updateWebsite(website, perSiteResult.site);
                            website.status = vm.statuses.checked;
                            website.has_network = perSiteResult.siteAdNetwork && perSiteResult.siteAdNetwork.is_enabled;
                        } else if (perSiteResult.error) {
                            website.status = vm.statuses.error;
                            website.error = perSiteResult.error;
                        }
                    }
                }
            }
        }

        function checkAgain(website) {
            website.status = vm.statuses.new;
            website.error = null;
            updateSites([website.url]);
        }

        function updateSites(urls) {
            publisherService.checkNetworkAndAdultWords(urls, vm.network.id)
                .success(function(result) {
                    handleCheckNetworkResult(result);
                })
                .error(function() {
                    console.log(arguments);
                    handleError(urls, 'Internal server error or problem with the connection.');
                });
        }

        function updateAllWebsites() {
            var urlsToCheck = [];
            for (var key in vm.websites) {
                if (vm.websites.hasOwnProperty(key)) {
                    vm.websites[key].status = vm.statuses.new;
                    urlsToCheck.push(vm.websites[key].url);
                    if (urlsToCheck.length == sizeWebsitesByRequest) {
                        updateSites(urlsToCheck);
                        urlsToCheck = [];
                    }
                }
            }
            updateSites(urlsToCheck);
        }

        function getNetwork(networkId) {
            for (var key in allNetworks) {
                if (allNetworks.hasOwnProperty(key)) {
                    if (allNetworks[key].id == networkId) {
                        return allNetworks[key];
                    }
                }
            }
            return null;
        }

        function init(sitesListContent, network, networks) {
            allNetworks = networks;
            vm.network = network;
            vm.switchNetworkId = ""+network.id;
            var sitesList = sitesListContent.split('\n');
            for (var key in sitesList) {
                if (sitesList.hasOwnProperty(key)) {
                    vm.websites.push({
                        url: sitesList[key],
                        status: vm.statuses.new,
                        is_adult: false,
                        adult_words: ''
                    });
                }
            }
            vm.$watch('switchNetworkId', function(value) {
                if (value != network.id && value != '') {
                    vm.network = getNetwork(value);
                    updateAllWebsites();
                }
            });
            updateAllWebsites();
        }
    }
})();
//# sourceMappingURL=app.js.map
