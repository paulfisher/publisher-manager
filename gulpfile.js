var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {

    mix.styles([
        'style.css',
        'bootstrap.min.css'
    ], 'public/compiled/styles.css', 'resources/assets/css');

    mix.scripts([
        'jquery/jquery-2.1.4.min.js',
        'angular/angular.min.js',
        'angular/ng-infinite-scroll.min.js',
        'bootstrap/bootstrap.min.js'
    ], 'public/compiled/vendor.js', 'resources/assets/js');

    mix.scripts([
        'app.js',
        'app/*.js'
    ], 'public/compiled/app.js', 'resources/assets/js');

    mix.version([
        'public/compiled/vendor.js',
        'public/compiled/app.js',
        'public/compiled/styles.css'
    ]);

});
