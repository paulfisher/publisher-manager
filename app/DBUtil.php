<?php namespace App;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class DBUtil {

    public static function concurrentDBUpdate($updateAction, $failureAction)
    {
        try {
            return $updateAction();
        } catch (QueryException $e) {
            if ($e->getCode() != 23505) throw $e;
            // concurrent inserting and unique constraint failure
            return $failureAction();
        }
    }

    public static function chunkIteration($query, $idColumn, $chunkSize, $callback)
    {
        $lastId = 0;
        $i = 0;
        while (true) {
            /** @var Builder $q */
            $q = clone $query;
            $res = $q->where($idColumn, '>', $lastId)
                ->orderBy($idColumn, 'asc')
                ->limit($chunkSize)
                ->get();
            $count = count($res);
            if ($count === 0) break;
            $lastId = $res[$count - 1]->{$idColumn};
            if ($callback($res, $i) === false) break;
            $i += $count;
        }
    }

    public static function hex($hex, $prefix = 'X')
    {
        return DB::raw("$prefix'$hex'");
    }

    public static function bitCount($hash, $prefix = 'X')
    {
        return DB::raw("LENGTH(REPLACE(CAST($prefix'$hash' as TEXT), '0', ''))");
    }
}