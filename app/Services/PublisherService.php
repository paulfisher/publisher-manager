<?php


namespace App\Services;


use App\Models\AdNetwork;
use App\Models\AdultWord;
use App\Models\Site;
use App\Models\SiteAdNetwork;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class PublisherService
{
    /**
     * @param Site $site
     * @param AdNetwork $network
     * @param $html
     * @return SiteAdNetwork|null
     */
    public function processSiteAdNetwork(Site $site, AdNetwork $network, $html = false)
    {
        $html = $html ?: $this->loadPublisherHtml($site->url);
        if (!$html) {
            return null;
        }
        $hasNetworkFootprint = $this->findNetworkFootprint($html, $network);

        /** @var SiteAdNetwork $siteAdNetwork */
        $siteAdNetwork = $site->siteAdNetworks()->where(['ad_network_id' => $network->id])->first();
        if ($hasNetworkFootprint) {
            if (!$siteAdNetwork) {
                $siteAdNetwork = $site->siteAdNetworks()->create(['ad_network_id' => $network->id, 'is_enabled' => true]);
                $siteAdNetwork->site->addLog($network->name . ' binding created and enabled');
            } else {
                $siteAdNetwork->update(['is_enabled' => true]);
                $siteAdNetwork->site->addLog($network->name . ' enabled. Footprint found');
            }
        } elseif ($siteAdNetwork) {
            $siteAdNetwork->update(['is_enabled' => false]);
            $siteAdNetwork->site->addLog($network->name . ' disabled. Footprint not found');
        }
        return $siteAdNetwork;
    }

    public function checkAdultWords($websiteUrl, $html)
    {
        /** @var Site $site */
        $site = Site::firstOrCreate(['url' => $websiteUrl]);

        $adultWords = $this->findAdultWords($html);

        $isAdult = !empty($adultWords);
        $adultWordsStr = implode(',', $adultWords);
        $site->update(['is_adult' => $isAdult, 'adult_words' => $adultWordsStr]);
        $site->addLog('checkAdultWords: ' . ($isAdult?'true':'false') . ($adultWordsStr?("\nadultWords: " . $adultWordsStr) : ''));

        return $site;
    }

    /*
[21:00:41] xBiz King: patterns
[21:00:43] xBiz King: adcash: java.php?option=rotateur
[21:01:07] xBiz King: popcash: popcash.net/pop.js
[21:01:19] xBiz King: propeller: apu.php?zoneid=
         * */
    private function findNetworkFootprint($html, AdNetwork $network)
    {
        $pattern = $network->pattern;
        if (strpos($html, $pattern) !== false) {
            return true;
        }

        return false;
    }

    private function findWordsInHtml($html, $words)
    {
        preg_match_all('/[>\s](' . implode('|', $words) .')[<\s]/', $html, $result);
        return isset($result[1]) ? array_unique($result[1]) : [];
    }

    private function findAdultWords($html)
    {
        $adultWords = AdultWord::all()->pluck('word')->toArray();
        if (empty($adultWords)) {
            $adultWords = ['sex', 'porn'];
        }
        return $this->findWordsInHtml($html, $adultWords);
    }

    public function loadPublisherHtml($url)
    {
        return @file_get_contents($url);
    }

    public function findSites($networkId, $showAdult, $showNotAdult, $offset = null, $limit = null)
    {
        $query = $this->findSitesQuery($networkId, $showAdult, $showNotAdult, $offset, $limit);
        return $query->get();
    }

    public function findSitesTotal($networkId, $showAdult, $showNotAdult)
    {
        $query = $this->findSitesQuery($networkId, $showAdult, $showNotAdult);
        return $query->select(DB::raw('count(1) as count'))->first()->count;
    }

    /**
     * @param $networkId
     * @param $showAdult
     * @param $showNotAdult
     * @param null $offset
     * @param null $limit
     * @return Builder
     */
    private function findSitesQuery($networkId, $showAdult, $showNotAdult, $offset = null, $limit = null)
    {
        if ($networkId > 0) {
            $query = DB::table('site')->join('site_ad_network', 'site.id', '=', 'site_ad_network.site_id')
                ->where('site_ad_network.ad_network_id', $networkId)
                ->select(['site.*', 'site_ad_network.is_enabled']);
        } else {
            /** @var Builder $query */
            $query = DB::table('site');
        }
        if ($showAdult) {
            if ($showNotAdult) {
                //showAdult and showNotAdult - show all
            } else {
                //showAdult
                $query->where(['is_adult' => true]);
            }
        } else {
            if ($showNotAdult) {
                //!showAdult and showNotAdult
                $query->where(['is_adult' => false]);
            } else {
                //!showAdult and !showNotAdult - show all
            }
        }
        if (!is_null($offset)) {
            $query->offset($offset);
        }
        if (!is_null($limit)) {
            $query->limit($limit);
        }
        return $query;
    }

}