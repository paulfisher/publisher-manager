<?php



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'SiteController@manageSites');


    Route::group(['prefix' => 'manager'], function () {
        Route::get('/', 'SiteController@manageSites');
        Route::get('update', 'SiteController@updateList');
        Route::post('update', 'SiteController@uploadList');
        Route::get('networks', 'SiteController@networks');
        Route::get('publisher/{site}', 'SiteController@viewSite');
        Route::get('publisher/{site}', 'SiteController@viewSite');
        Route::post('publisher/checkNetwork', 'SiteController@publisherCheckNetwork');

        Route::get('adultWords', 'SiteController@adultWords');
        Route::post('adultWords', 'SiteController@uploadAdultWords');
    });

    Route::group(['prefix' => 'api'], function () {
        Route::post('checkNetworkAndAdultWords', 'ApiController@checkNetworkAndAdultWords');
        Route::post('publisher/markSiteAdult', 'ApiController@markSiteAdult');
        Route::post('publisher/toggleSiteAdult', 'ApiController@toggleSiteAdult');
        Route::post('network/save', 'ApiController@saveNetwork');
        Route::post('network/delete', 'ApiController@deleteNetwork');
        Route::post('sites/find', 'ApiController@findSites');
        Route::post('publisher/save', 'ApiController@savePublisher');
    });
});
