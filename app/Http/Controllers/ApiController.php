<?php

namespace App\Http\Controllers;

use App\Models\AdNetwork;
use App\Models\AdultWord;
use App\Models\Site;
use App\Models\SiteAdNetwork;
use App\Services\PublisherService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ApiController extends Controller
{
    public function toggleSiteAdult()
    {
        $siteId = Input::get('id');
        /** @var Site $site */
        $site = Site::findOrFail($siteId);
        $site->is_adult = !$site->is_adult;
        $site->save();

        $site->addLog('Toggle is_adult: ' . ($site->is_adult?'true':'false'));

        return $site;
    }

    public function markSiteAdult()
    {
        $url = Input::get('url');
        $isAdult = Input::get('isAdult');
        /** @var Site $site */
        $site = Site::where(['url' => $url])->first();
        if ($site) {
            $site->update(['is_adult' => $isAdult]);
            $site->addLog('Manual mark is_adult: ' . ($isAdult?'true':'false'));
        }
        return $site;
    }

    public function checkNetworkAndAdultWords(PublisherService $publisherService)
    {
        set_time_limit(null);
        /** @var AdNetwork $network */
        $network = AdNetwork::findOrFail(Input::get('network'));
        $websiteUrls = Input::get('urls');
        $result = [];

        foreach ($websiteUrls as $websiteUrl) {
            $html = $publisherService->loadPublisherHtml($websiteUrl);
            if (!$html) {
                $result[$websiteUrl] = [
                    'error' => 'Error while loading html from ' . $websiteUrl,
                ];
                continue;
            }
            $site = $publisherService->checkAdultWords($websiteUrl, $html);
            $siteAdNetwork = $publisherService->processSiteAdNetwork($site, $network, $html);

            $result[$websiteUrl] = [
                'site' => $site->toArray(),
                'siteAdNetwork' => $siteAdNetwork ? $siteAdNetwork->toArray() : []
            ];
        }
        return $result;
    }

    public function saveNetwork()
    {
        $id = intval(Input::get('id'), 10);
        $name = Input::get('name');
        $pattern = Input::get('pattern');

        if ($id > 0) {
            DB::table('ad_network')
                ->where('id', $id)
                ->update(['name' => $name, 'pattern' => $pattern]);
        } else {
            $byName = AdNetwork::where('name', $name)->first();
            if ($byName) {
                $id = $byName->id;
                DB::table('ad_network')
                    ->where('id', $id)
                    ->update(['name' => $name, 'pattern' => $pattern, 'is_deleted' => false]);
            } else {
                $id = DB::table('ad_network')->insertGetId(
                    ['name' => $name, 'pattern' => $pattern]
                );
            }

        }
        return AdNetwork::findOrFail($id);
    }

    public function deleteNetwork()
    {
        $id = intval(Input::get('id'), 10);
        if ($id > 0) {
            DB::table('ad_network')
                ->where('id', $id)
                ->update(['is_deleted' => true]);
        }
        return AdNetwork::findOrFail($id);
    }

    public function findSites(PublisherService $publisherService)
    {
        set_time_limit(null);
        $networkId = intval(Input::get('networkId'), 10);
        $showAdult = Input::get('showAdult');
        $showNotAdult = Input::get('showNotAdult');
        $limit = Input::get('limit', 50);
        $offset = Input::get('offset', 0);

        $sites = $publisherService->findSites($networkId, $showAdult, $showNotAdult, $offset, $limit);
        $total = $publisherService->findSitesTotal($networkId, $showAdult, $showNotAdult);

        return [
            'sites' => $sites,
            'total' => $total,
            'limit' => $limit,
            'offset' => $offset
        ];
    }

    public function savePublisher()
    {
        $siteId = intval(Input::get('id'), 10);
        $url = Input::get('url');
        if (empty($url)) {
            return ['error' => 'empty url'];
        }

        $site = null;
        if ($siteId > 0) {
            $site = Site::findOrFail($siteId);
            $site->update(['url' => $url]);
        } else {
            $site = Site::create(['url' => $url, 'is_adult' => false]);
        }
        return $site;
    }
}
