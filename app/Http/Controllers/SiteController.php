<?php

namespace App\Http\Controllers;

use App\Models\AdNetwork;
use App\Models\AdultWord;
use App\Models\Site;
use App\Models\SiteAdNetwork;
use App\Services\PublisherService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SiteController extends Controller
{
    public function manageSites()
    {
        $networks = AdNetwork::enabled()->get();
        $networksData = AdNetwork::enabled()->pluck('name', 'id')->toArray();
        return view('manage-sites', compact('networks', 'networksData'));
    }

    public function updateList()
    {
        $networks = AdNetwork::enabled()->pluck('name', 'id');
        return view('upload-list', compact('networks'));
    }

    public function adultWords()
    {
        $words = AdultWord::all();
        return view('adult-words', compact('words'));
    }

    public function uploadAdultWords()
    {
        /** @var UploadedFile $file */
        $file = Input::file('list');
        if (!$file) {
            return Redirect::action('SiteController@adultWords');
        }
        $listContent = file_get_contents($file->getRealPath());
        $words = explode("\n", $listContent);
        DB::table('adult_word')->delete();
        foreach ($words as $word) {
            AdultWord::updateOrCreate(['word' => $word]);
        }
        return Redirect::action('SiteController@adultWords');
    }

    public function uploadList()
    {
        /** @var UploadedFile $file */
        $file = Input::file('list');
        if (!$file) {
            return Redirect::action('SiteController@updateList');
        }
        /** @var AdNetwork $network */
        $network = AdNetwork::findOrFail(Input::get('network'));
        $networks = AdNetwork::enabled()->pluck('name', 'id');
        $allNetworks = AdNetwork::enabled()->get();
        $listContent = file_get_contents($file->getRealPath());

        return view('uploaded-list', compact('listContent', 'network', 'networks', 'allNetworks'));
    }

    public function networks()
    {
        $networks = AdNetwork::all();
        return view('networks', compact('networks'));
    }

    public function viewSite(Site $site)
    {
        $checkNetworkResult = session('checkNetworkResult');
        $networks = AdNetwork::enabled()->pluck('name', 'id');
        return view('site', compact('site', 'networks', 'checkNetworkResult'));
    }

    public function publisherCheckNetwork(PublisherService $publisherService)
    {
        set_time_limit(null);
        $siteId = Input::get('siteId');
        $networkId = Input::get('network');
        $site = Site::findOrFail($siteId);
        $network = AdNetwork::findOrFail($networkId);
        $siteAdNetwork = $publisherService->processSiteAdNetwork($site, $network);

        return redirect()
            ->action('SiteController@viewSite', $siteId)
            ->with('checkNetworkResult', $siteAdNetwork && $siteAdNetwork->is_enabled ? 'success' : $network->name . ' not found');
    }
}