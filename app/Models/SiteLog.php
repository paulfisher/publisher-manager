<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string url
 * @property boolean is_adult
 * @property string adult_words
 */
class SiteLog extends Model
{
    protected $table = 'site_log';

    protected $fillable = ['site_id', 'data'];


    public function site()
    {
        return $this->belongsTo('App\Models\Site');
    }
}