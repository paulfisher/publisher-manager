<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property boolean is_enabled
 * @property Site site
 * @property AdNetwork adNetwork
 */
class SiteAdNetwork extends Model
{
    protected $table = 'site_ad_network';

    protected $fillable = ['site_id', 'ad_network_id', 'is_enabled'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo('App\Models\Site');
    }

    public function adNetwork()
    {
        return $this->belongsTo('App\Models\AdNetwork');
    }
}