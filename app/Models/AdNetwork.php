<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string pattern
 * @property string name
 */
class AdNetwork extends Model
{
    protected $table = 'ad_network';

    protected $fillable = ['name', 'pattern', 'is_deleted'];

    public function scopeEnabled($query) {
        $query->where('is_deleted', false);
    }
}
