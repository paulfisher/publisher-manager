<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string url
 * @property boolean is_adult
 * @property string adult_words
 *
 * @property SiteLog[] logs
 */
class Site extends Model
{
    protected $table = 'site';

    protected $fillable = ['url', 'is_adult', 'adult_words'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siteAdNetworks()
    {
        return $this->hasMany('App\Models\SiteAdNetwork');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany('App\Models\SiteLog');
    }

    public function addLog($log)
    {
        $this->logs()->create(['data' => $log]);
    }
}
