<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdultWord extends Model
{
    protected $table = 'adult_word';

    protected $fillable = ['word'];

    public $timestamps = false;
}
