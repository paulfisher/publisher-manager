<?php

namespace App\Console\Commands;

use App\DBUtil;
use App\Models\AdNetwork;
use App\Models\Site;
use App\Models\SiteAdNetwork;
use App\ProgressLogger;
use App\Services\PublisherService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckPublishers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publishers:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param PublisherService $publisherService
     * @return mixed
     */
    public function handle(PublisherService $publisherService)
    {
        $websitesQuery = SiteAdNetwork::query()->select(['id', 'site_id', 'ad_network_id']);

        $networks = AdNetwork::all();

        $progress = new ProgressLogger();
        $progress->total = SiteAdNetwork::query()
            ->select(DB::raw('count(id) as count'))
            ->first()['count'];

        DBUtil::chunkIteration($websitesQuery, 'id', 100, function($siteAdNetworks) use ($publisherService, $networks, $progress) {
            /** @var SiteAdNetwork $siteAdNetwork */
            foreach ($siteAdNetworks as $siteAdNetwork) {
                $publisherService->processSiteAdNetwork($siteAdNetwork->site, $siteAdNetwork->adNetwork);
            }
            $progress->processed += count($siteAdNetworks);
            $progress->updated += count($siteAdNetworks);
            $progress->update();
        });
    }
}
