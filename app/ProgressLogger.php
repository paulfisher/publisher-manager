<?php
namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class ProgressLogger {
    public $total;
    public $processed;
    public $updated;
    private $message;

    public function __construct()
    {
        $this->total = $this->processed = $this->updated = 0;
    }

    public function log($message)
    {
        $message = call_user_func_array('sprintf', func_get_args());
        Log::info($message);
        //$this->update($message);
    }

    public function update($message = null)
    {
        if ($message !== null) $this->message = $message;
        if (App::runningInConsole()) {
            if (is_null($this->message) || $this->message === '') {
                echo sprintf("\r[p:%d/u:%d/t:%d]", $this->processed, $this->updated, $this->total);
            } else {
                echo sprintf("\r%s\r[p:%d/u:%d/t:%d] %s", str_repeat(' ', 100), $this->processed, $this->updated, $this->total, $this->message);
            }
        }
    }
}