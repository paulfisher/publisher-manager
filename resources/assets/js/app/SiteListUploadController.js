;(function() {
    angular.module('PublisherManager')
        .controller('siteListUploadController', SiteListUploadController);

    SiteListUploadController.$inject = ['$scope', 'publisherService'];
    function SiteListUploadController(scope, publisherService) {
        var vm = scope;

        var sizeWebsitesByRequest = 20;
        var allNetworks = [];

        vm.init = init;
        vm.change = change;
        vm.cancelChange = cancelChange;
        vm.save = save;
        vm.checkAgain = checkAgain;

        vm.statuses = {
            'new' : 'new',
            checked : 'checked',
            error : 'error'
        };

        vm.network = null;
        vm.websites = [];
        vm.switchNetworkId = '';

        function save(website) {
            publisherService.markSiteAdult(website.url, website.is_adult)
                .success(function(result) {
                    updateWebsite(website, result);
                    website.is_changed = false;
                });
        }

        function cancelChange(website) {
            website.is_changed = false;
            website.is_adult = website.server_is_adult;
        }

        function change(website) {
            website.is_changed = true;
        }

        function getWebsiteByUrl(url) {
            for (var key in vm.websites) {
                if (vm.websites.hasOwnProperty(key)) {
                    if (vm.websites[key].url == url) {
                        return vm.websites[key];
                    }
                }
            }
            return null;
        }

        function updateWebsite(website, data) {
            website.id = data.id;
            website.is_adult = data.is_adult;
            website.server_is_adult = data.is_adult;
            website.adult_words = data.adult_words;
        }

        function handleError(urls, error) {
            for (var key in urls) {
                if (urls.hasOwnProperty(key)) {
                    var website = getWebsiteByUrl(urls[key]);
                    if (website) {
                        website.status = vm.statuses.error;
                        website.error = error;
                    }
                }
            }
        }

        function handleCheckNetworkResult(checkNetworkAndAdultWordsResult) {
            for (var url in checkNetworkAndAdultWordsResult) {
                if (checkNetworkAndAdultWordsResult.hasOwnProperty(url)) {
                    var perSiteResult = checkNetworkAndAdultWordsResult[url];
                    var website = getWebsiteByUrl(url);
                    if (website) {
                        if (perSiteResult.site) {
                            updateWebsite(website, perSiteResult.site);
                            website.status = vm.statuses.checked;
                            website.has_network = perSiteResult.siteAdNetwork && perSiteResult.siteAdNetwork.is_enabled;
                        } else if (perSiteResult.error) {
                            website.status = vm.statuses.error;
                            website.error = perSiteResult.error;
                        }
                    }
                }
            }
        }

        function checkAgain(website) {
            website.status = vm.statuses.new;
            website.error = null;
            updateSites([website.url]);
        }

        function updateSites(urls) {
            publisherService.checkNetworkAndAdultWords(urls, vm.network.id)
                .success(function(result) {
                    handleCheckNetworkResult(result);
                })
                .error(function() {
                    console.log(arguments);
                    handleError(urls, 'Internal server error or problem with the connection.');
                });
        }

        function updateAllWebsites() {
            var urlsToCheck = [];
            for (var key in vm.websites) {
                if (vm.websites.hasOwnProperty(key)) {
                    vm.websites[key].status = vm.statuses.new;
                    urlsToCheck.push(vm.websites[key].url);
                    if (urlsToCheck.length == sizeWebsitesByRequest) {
                        updateSites(urlsToCheck);
                        urlsToCheck = [];
                    }
                }
            }
            updateSites(urlsToCheck);
        }

        function getNetwork(networkId) {
            for (var key in allNetworks) {
                if (allNetworks.hasOwnProperty(key)) {
                    if (allNetworks[key].id == networkId) {
                        return allNetworks[key];
                    }
                }
            }
            return null;
        }

        function init(sitesListContent, network, networks) {
            allNetworks = networks;
            vm.network = network;
            vm.switchNetworkId = ""+network.id;
            var sitesList = sitesListContent.split('\n');
            for (var key in sitesList) {
                if (sitesList.hasOwnProperty(key)) {
                    vm.websites.push({
                        url: sitesList[key],
                        status: vm.statuses.new,
                        is_adult: false,
                        adult_words: ''
                    });
                }
            }
            vm.$watch('switchNetworkId', function(value) {
                if (value != network.id && value != '') {
                    vm.network = getNetwork(value);
                    updateAllWebsites();
                }
            });
            updateAllWebsites();
        }
    }
})();