;(function() {
    angular.module('PublisherManager')
        .controller('networkListController', NetworkListController);

    NetworkListController.$inject = ['$scope', 'publisherService'];
    function NetworkListController(scope, publisherService) {
        var vm = scope;

        vm.init = init;
        vm.edit = edit;
        vm.cancelEdit = cancelEdit;
        vm.save = save;
        vm.deleteNetwork = deleteNetwork;
        vm.add = add;
        vm.networks = [];

        function init(networks) {
            vm.networks = networks;
        }

        function deleteNetwork(network) {
            if (confirm('delete network ' + network.name + '?')) {
                network.saving = true;
                publisherService.deleteNetwork(network.id)
                    .success(function(result) {
                        network.is_deleted = true;
                        network.saving = false;
                    });
            }
        }

        function edit(network) {
            network.edit = true;
            network.originalPattern = network.pattern;
            network.originalName = network.name;
        }

        function cancelEdit(network) {
            if (!network.id) {
                vm.networks.splice(vm.networks.length - 1, 1);
                return;
            }
            network.edit = false;
            network.pattern = network.originalPattern ? network.originalPattern : network.pattern;
            network.name = network.originalName ? network.originalName : network.name;
        }

        function add() {
            vm.networks.push({
                name : 'new network',
                pattern : '',
                edit : true
            });
        }

        function save(network) {
            network.edit = false;
            network.saving = true;
            publisherService.saveNetwork(network)
                .success(function(result) {
                    if (!network.id) {
                        network.id = result.id;
                    }
                    network.name = result.name;
                    network.pattern = result.pattern;
                    network.originalPattern = network.pattern;
                    network.originalName = network.name;
                    network.saving = false;
                });
        }
    }
})();