;(function() {
    angular.module('PublisherManager')
        .controller('publishersListController', PublishersListController);

    PublishersListController.$inject = ['$scope', 'publisherService'];
    function PublishersListController(scope, publisherService) {
        var vm = scope;

        vm.init = init;
        vm.nextPage = nextPage;
        vm.showPublisherInfo = showPublisherInfo;
        vm.addPublisher = addPublisher;

        vm.edit = edit;
        vm.save = save;
        vm.cancelEdit = cancelEdit;
        vm.toggleAdult = toggleAdult;

        vm.networks = [];
        vm.publishers = [];
        vm.showAdult = false;
        vm.showNotAdult = true;
        vm.networkId = null;

        vm.pagination = {
            offset: 0,
            limit: 50
        };
        vm.loading = false;

        var allLoaded = false;

        function toggleAdult(publisher) {
            publisherService.toggleSiteAdult(publisher.id)
                .success(function(result) {
                    publisher.isAdult = result.is_adult;
                });
        }

        function save(publisher) {
            publisherService.savePublisher(publisher)
                .success(function(result) {
                    if (result.id) {
                        publisher.id = result.id;
                        publisher.url = result.url;
                        publisher.isAdult = result.is_adult;
                        publisher.adultWords = result.adult_words;
                        publisher.edit = false;
                    } else {
                        cancelEdit(publisher);
                        alert('some error');
                        console.log(result);
                    }
                });
        }

        function edit(publisher) {
            publisher.edit = true;
        }

        function cancelEdit(publisher) {
            publisher.edit = false;
        }

        function showPublisherInfo(publisher) {
            window.open('/manager/publisher/' + publisher.id, '_blank').focus();
        }

        function addPublisher(site) {
            if (site) {
                vm.publishers.push({
                    id: site.id,
                    url: site.url,
                    isAdult: site.is_adult != '0',
                    adultWords: site.adult_words
                });
            } else {
                vm.publishers.push({
                    id: 0,
                    url: '',
                    isAdult: false,
                    adultWords: '',
                    edit: true
                });
            }
        }

        function handleLoadedPublishers(result) {
            for (var key in result.sites) {
                if (result.sites.hasOwnProperty(key)) {
                    addPublisher(result.sites[key]);
                }
            }
            vm.pagination.total = result.total;
            if (result.total >= result.offset + result.limit) {
                vm.pagination.offset += result.limit;
            } else {
                allLoaded = true;
            }
        }

        function loadPublishers() {
            if (vm.loading) return;
            vm.loading = true;
            publisherService.findSites({
                networkId: vm.networkId,
                showAdult: vm.showAdult,
                showNotAdult: vm.showNotAdult,
                limit: vm.pagination.limit,
                offset: vm.pagination.offset
            })
                .success(function(result) {
                    handleLoadedPublishers(result);
                    vm.loading = false;
                });
        }

        function nextPage() {
            if (!allLoaded) {
                loadPublishers();
            }
        }

        function onFilterChanged(newValue, oldValue) {
            if (newValue != oldValue) {
                vm.pagination.offset = 0;
                vm.pagination.total = 0;
                allLoaded = false;
                vm.publishers = [];
                loadPublishers();
            }
        }

        function init(networks) {
            loadPublishers();

            vm.networks = networks;

            vm.$watch('networkId', onFilterChanged);
            vm.$watch('showAdult', onFilterChanged);
            vm.$watch('showNotAdult', onFilterChanged);
        }
    }
})();