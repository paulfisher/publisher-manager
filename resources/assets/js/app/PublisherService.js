;(function() {
    angular.module('PublisherManager')
        .constant('PUBLISHERS_API', {
            checkNetworkAndAdultWords: '/api/checkNetworkAndAdultWords',
            markSiteAdult: '/api/publisher/markSiteAdult',
            toggleSiteAdult: '/api/publisher/toggleSiteAdult',
            saveNetwork: '/api/network/save',
            deleteNetwork: '/api/network/delete',
            findSites: '/api/sites/find',
            savePublisher: '/api/publisher/save'
        })
        .factory('publisherService', PublisherService);

    PublisherService.$inject = ['PUBLISHERS_API', '$http'];
    function PublisherService(publishersApi, http) {
        return {
            markSiteAdult: markSiteAdult,
            toggleSiteAdult: toggleSiteAdult,
            checkNetworkAndAdultWords: checkNetworkAndAdultWords,
            saveNetwork: saveNetwork,
            deleteNetwork: deleteNetwork,
            findSites: findSites,
            savePublisher: savePublisher
        };

        function checkNetworkAndAdultWords(urls, network) {
            return http.post(publishersApi.checkNetworkAndAdultWords, {urls: urls, network: network});
        }

        function markSiteAdult(url, isAdult) {
            return http.post(publishersApi.markSiteAdult, {url: url, isAdult: isAdult});
        }

        function toggleSiteAdult(siteId) {
            return http.post(publishersApi.toggleSiteAdult, {id: siteId});
        }

        function saveNetwork(network) {
            return http.post(publishersApi.saveNetwork, network);
        }

        function deleteNetwork(networkId) {
            return http.post(publishersApi.deleteNetwork, {id: networkId});
        }

        function findSites(filter) {
            return http.post(publishersApi.findSites, filter);
        }

        function savePublisher(publisher) {
            return http.post(publishersApi.savePublisher, publisher);
        }
    }

})();