@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="page-header">
                <h4>Upload publishers list</h4>
            </div>
            <div class="form form-inline">
                {!! Form::open(['url' => 'manager/update', 'method'=>'POST', 'files'=>true]) !!}
                <div class="form-group">
                    {!! Form::file('list', ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! Form::select('network', $networks, null, ['class' => 'form-control', 'placeholder' => 'Select network', 'required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Submit', ['class' => 'form-control']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop