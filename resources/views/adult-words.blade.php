@extends('app')

@section('content')
    <div class="row" data-ng-cloak="">
        <div class="col-md-12">
            <div class="page-header">
                <h4>Adult words</h4>
            </div>
            <a href="javascript: void 0;" data-ng-click="showUploadFileForm = !showUploadFileForm">upload file with words</a>
            <div class="form form-inline" data-ng-show="showUploadFileForm" style="margin-top: 20px;margin-bottom: 20px;">
                {!! Form::open(['url' => 'manager/adultWords', 'method'=>'POST', 'files'=>true]) !!}
                <div class="form-group">
                    {!! Form::file('list', ['class' => 'form-control', 'required' => 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Submit', ['class' => 'form-control']) !!}
                    <a href="javascript: void 0;" data-ng-click="showUploadFileForm = false" style="margin-left: 10px;">cancel</a>
                </div>
                {!! Form::close() !!}
            </div>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Word</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($words as $word)
                <tr>
                    <td>
                        {{$word->word}}
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop