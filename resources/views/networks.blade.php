@extends('app')

@section('content')
    <div class="row" data-ng-controller="networkListController" data-ng-init='init({!! $networks !!})' data-ng-cloak="">
        <div class="col-md-12">

            <div class="page-header">
                <h4>Networks</h4>
            </div>
            <a class="btn btn-sm btn-success" href="javascript: void 0;" data-ng-click="add()">+ add network</a>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Network</th>
                    <th>Pattern</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr data-ng-repeat="network in networks"  data-ng-show="network.is_deleted != 1">
                    <td>
                        <p data-ng-click="edit(network)" data-ng-show="!network.edit">
                            @{{ network.name }}
                        </p>
                        <textarea data-ng-show="network.edit" data-ng-model="network.name"></textarea>
                        <div class="loading" data-ng-show="network.saving"></div>
                    </td>
                    <td>
                        <p data-ng-click="edit(network)" data-ng-show="!network.edit">
                            @{{ network.pattern }}
                        </p>
                        <textarea data-ng-show="network.edit" data-ng-model="network.pattern"></textarea>
                    </td>
                    <td>
                        <div data-ng-show="network.edit">
                            <button class="btn btn-xs btn-primary" data-ng-click="save(network)">save</button>
                            <a href="javascript: void 0;" data-ng-click="cancelEdit(network)" style="margin-right: 20px;">cancel</a>

                            <a href="javascript: void 0;" class="btn btn-xs btn-danger" data-ng-show="network.id > 0" data-ng-click="deleteNetwork(network)">delete</a>
                        </div>

                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop