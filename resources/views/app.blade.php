<!doctype html>
<html lang="en" data-ng-app="PublisherManager">
<head>
    <meta charset="UTF-8">
    <title>Publisher manager</title>

    <link href='{{ elixir('compiled/styles.css') }}' rel='stylesheet' type='text/css'/>

    @yield('head')

</head>
<body>
@include('partials.nav')


<div id="content">
    <div class="container">
        @yield('content')
    </div>
</div>

@yield('footer')

<script src="{{ elixir('compiled/vendor.js') }}" type="text/javascript"></script>
@yield('scripts')
<script src="{{ elixir('compiled/app.js') }}" type="text/javascript"></script>

</body>
</html>