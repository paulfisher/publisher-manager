<style>
    .navbar a {
        color: #3a3f58;
        background: #FFFFFF;
    }
    .navbar a.navbar-brand {
        background: #FFFFFF;
    }


</style>

<nav class="navbar navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                Publisher manager
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>{!! link_to('manager', 'Manager') !!}</li>
                <li>{!! link_to('manager/networks', 'Networks') !!}</li>
                <li>{!! link_to('manager/adultWords', 'Adult words') !!}</li>
            </ul>
        </div>

    </div>
</nav>