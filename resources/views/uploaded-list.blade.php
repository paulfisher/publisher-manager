@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12" data-ng-controller="siteListUploadController" data-ng-init='init("{{$listContent}}", {!! $network !!}, {!! $allNetworks !!})' data-ng-cloak="">
            <div class="page-header">
                <h4>Publishers check for @{{ network.name }}</h4>
            </div>

            <div class="row">
                <div class="col-md-push-11 col-md-1">
                    <a href="javascript: void 0;" data-ng-click="showOptions = !showOptions"><small>options</small></a>
                </div>
            </div>
            <div class="row" data-ng-show="showOptions">
                <div class="col-md-2">
                    <div class="form-inline">
                        <a href="javascript: void 0;" data-ng-click="changeNetwork = !changeNetwork" data-ng-show="!changeNetwork">apply list for another network</a>
                        <div class="form-group" data-ng-show="changeNetwork">
                            {!! Form::select('network', $networks, $network->id, ['data-ng-model' => 'switchNetworkId', 'class' => 'form-control']) !!}
                            <a href="javascript: void 0;" data-ng-click="changeNetwork = false" style="margin-left:10px;">cancel</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <a href="javascript: void 0;" data-ng-click="showOnlyFound = true" data-ng-show="!showOnlyFound">show only if pattern found</a>
                    <a href="javascript: void 0;" data-ng-click="showOnlyFound = false" data-ng-show="showOnlyFound">show all</a>
                </div>
                <div class="col-md-8">
                    <a href="javascript: void 0;" data-ng-click="uploadNewFile = !uploadNewFile" data-ng-show="!uploadNewFile">upload another file</a>
                    <div class="form form-inline" data-ng-show="uploadNewFile">
                        {!! Form::open(['url' => 'manager/update', 'method'=>'POST', 'files'=>true]) !!}
                        <div class="form-group">
                            {!! Form::file('list', ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! Form::select('network', $networks, null, ['class' => 'form-control', 'placeholder' => 'Select network', 'required' => 'required']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Submit', ['class' => 'form-control']) !!}
                            <a href="javascript: void 0;" data-ng-click="uploadNewFile = false" style="margin-left:10px;">cancel</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <hr data-ng-show="showOptions"/>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width: 250px;">Site's url</th>
                    <th style="width: 150px;">Adult</th>
                    <th>Found adult words</th>
                    <th>@{{ network.name }} script check</th>
                </tr>
                </thead>
                <tbody>
                <tr data-ng-repeat="website in websites" data-ng-show="!showOnlyFound || website.has_network">
                    <td>
                        <a href="@{{website.url}}" target="_blank">@{{website.url}}</a>
                        <span data-ng-if="website.status == statuses.new" class="loading"></span>
                    </td>
                    <td>
                        <label data-ng-if="website.status == statuses.checked">
                            <input type="checkbox" data-ng-model="website.is_adult" data-ng-change="change(website)"/>
                        </label>
                        <span data-ng-show="website.is_changed">
                            <button class="btn btn-primary btn-xs" data-ng-click="save(website)">save</button>
                            <a href="javascript: void 0;" data-ng-click="cancelChange(website)">cancel</a>
                        </span>
                    </td>
                    <td>
                        <span data-ng-if="website.is_adult && website.status == statuses.checked">@{{ website.adult_words }}</span>
                    </td>
                    <td>
                        <span data-ng-if="website.status == statuses.checked">
                            <span data-ng-if="website.has_network" class="text-success">@{{ network.pattern }}: pattern found</span>
                            <span data-ng-if="!website.has_network">pattern is not found</span>
                        </span>
                        <div data-ng-if="website.status == statuses.error">
                            <small class="text-warning">@{{ website.error }}</small>
                            <br/>
                            <a href="javascript: void 0;" data-ng-click="checkAgain(website)">try again</a>
                        </div>

                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
@stop