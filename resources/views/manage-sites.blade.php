@extends('app')

@section('content')
    <div class="row" data-ng-controller="publishersListController" data-ng-init='init({!! $networks !!})'>
        <div class="col-md-12">
            <div class="page-header">
                <h4>Publishers</h4>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {!! Form::select('network', $networksData, null, ['data-ng-model' => 'networkId', 'class' => 'form-control', 'placeholder' => 'Filter by network']) !!}
                </div>
                <div class="col-md-2">
                    <label>
                        Show adult <input type="checkbox" data-ng-model="showAdult"/>
                    </label>
                    <label>
                        Show not-adult <input type="checkbox" data-ng-model="showNotAdult"/>
                    </label>
                </div>
                <div class="col-md-2">
                    <a href="javascript: void 0;" data-ng-click="addPublisher()" class="btn btn-sm btn-info">+ add publisher</a>
                </div>
                <div class="col-md-push-4 col-md-2">
                    <a href="{{action('SiteController@uploadList')}}" class="btn btn-sm btn-success">+ upload publishers list file</a>
                </div>
            </div>

            <div data-infinite-scroll="nextPage()" data-infinite-scroll-disabled="loading">
                <table class="table table-hover table-responsive">
                    <thead>
                    <tr>
                        <th>
                            Publisher
                        </th>
                        <th>
                            Adult
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr data-ng-repeat="publisher in publishers">
                        <td>
                            <a style="margin-right: 14px;" href="javascript: void 0;" data-ng-click="publisher.showOptions = !publisher.showOptions">
                                @{{publisher.url}}
                            </a>
                            <span data-ng-show="publisher.showOptions" style="margin-right: 14px;">
                                <a href="@{{publisher.url}}" class="btn btn-sm btn-default" target="_blank">open url in new window</a>
                                <a href="javascript: void 0;" class="btn btn-sm btn-default" data-ng-click="showPublisherInfo(publisher)" data-ng-show="publisher.id > 0">info</a>
                                <a href="javascript: void 0;" class="btn btn-sm btn-default" data-ng-click="edit(publisher)" data-ng-show="publisher.id > 0">edit</a>

                                <a href="javascript: void 0;" class="btn btn-sm btn-danger" data-ng-show="publisher.id > 0 && !publisher.isAdult" data-ng-click="toggleAdult(publisher)">mark adult</a>
                                <a href="javascript: void 0;" class="btn btn-sm btn-success" data-ng-show="publisher.id > 0 && publisher.isAdult" data-ng-click="toggleAdult(publisher)">mark not adult</a>
                            </span>
                            <span data-ng-show="publisher.edit">
                                <input type="text" data-ng-model="publisher.url"/>
                                <a class="btn btn-xs btn-primary" data-ng-click="save(publisher)">save</a>
                                <a style="margin-left: 4px;" data-ng-click="cancelEdit(publisher)">cancel</a>
                            </span>
                        </td>
                        <td>
                            <span data-ng-show="publisher.isAdult" class="text-warning">
                                adult
                                <small data-ng-show="publisher.adultWords">(@{{ publisher.adultWords }})</small>
                            </span>
                            <span data-ng-show="!publisher.isAdult" class="text-success">not adult</span>
                        </td>
                    </tr>
                    <tr data-ng-show="loading">
                        <td colspan="2">
                            <div class="loading"></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>


        </div>
    </div>
@stop