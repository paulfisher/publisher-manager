@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="page-header">
                <h4><a href="{{$site->url}}" target="_blank">{{$site->url}}</a></h4>
                @if ($site->is_adult)
                    <span class="text-warning">adult</span>
                    @if ($site->adult_words)
                        <small class="text-muted">({{$site->adult_words}})</small>
                    @endif
                @else
                    <span class="text-success">not adult</span>
                @endif
            </div>

            <div class="row">
                <div class="col-md-8">
                    <h4>Networks:</h4>
                    <ul class="list-group">
                        @foreach ($site->siteAdNetworks as $siteAdNetwork)
                            <li class="list-group-item @if ($siteAdNetwork->is_enabled) text-success @else text-warning @endif">
                                <strong>{{$siteAdNetwork->adNetwork->name}}</strong>
                                @if ($siteAdNetwork->is_enabled)
                                    enabled
                                @else
                                    disabled
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            @if ($checkNetworkResult && $checkNetworkResult != 'success')
                <div class="alert alert-danger">
                    {{$checkNetworkResult}}
                </div>
            @endif

            <a href="javascript: void 0;" data-ng-show="!showCheckNetworkForm" data-ng-click="showCheckNetworkForm = !showCheckNetworkForm">check network</a>
            <div class="row" data-ng-show="showCheckNetworkForm">
                <div class="col-md-8">
                    <div class="form form-inline">
                        {!! Form::open(['url' => 'manager/publisher/checkNetwork', 'method'=>'POST']) !!}
                        <div class="form-group">
                            {!! Form::hidden('siteId', $site->id) !!}
                            {!! Form::select('network', $networks, null, ['class' => 'form-control', 'placeholder' => 'Select network', 'required' => 'required']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Submit', ['class' => 'form-control']) !!}
                            <a href="javascript: void 0;" data-ng-click="showCheckNetworkForm = false" style="margin-left: 14px;">cancel</a>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <hr/>

            <div class="row">
                <div class="col-md-8">
                    <h4>Logs:</h4>
                    <ul class="list-group">
                        @foreach ($site->logs as $log)
                            <li class="list-group-item">
                                {!! nl2br($log->data) !!}
                                <span class="badge">
                                    {{$log->created_at}}
                                </span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop