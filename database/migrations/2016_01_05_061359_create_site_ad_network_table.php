<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteAdNetworkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_ad_network', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned();
            $table->integer('ad_network_id')->unsigned();
            $table->boolean('is_enabled');
            $table->timestamps();

            $table->foreign('site_id', 'fk__site_ad_network__site_id')->references('id')->on('site');
            $table->foreign('ad_network_id', 'fk__site_ad_network__ad_network_id')->references('id')->on('ad_network');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_ad_network');
    }
}
